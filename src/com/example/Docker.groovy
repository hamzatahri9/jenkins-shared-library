#!/usr/bin/env groovy

package com.example

class Docker implements Serializable {

    def script
    
    Docker(script) {
        this.script = script
    }

    def buildDockerImage(String imageName) {
        script.echo "building the docker image..."
        script.withCredentials([script.usernamePassword(credentialsId: 'Docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            script.sh """
                docker build -t ${imageName} .
                echo \${PASS} | docker login -u \${USER} --password-stdin
                docker push ${imageName}
            """
        }
    }
}
